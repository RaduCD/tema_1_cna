#include "ShowNameServiceImpl.h"

::grpc::Status ShowNameServiceImpl::ShowName(::grpc::ServerContext* context, const::ShowNameRequest* request, ::Empty* response)
{
	std::string clientName = request->clientname();
	std::cout  << "Hello, "<< clientName << "\n";
	return ::grpc::Status::OK;
}
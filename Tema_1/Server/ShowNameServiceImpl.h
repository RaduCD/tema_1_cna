#pragma once
#include"SumOperation.grpc.pb.h"
#include "ShowNameOperation.grpc.pb.h"

class ShowNameServiceImpl  final : public ShowNameOperationService::Service
{
	
	public:
		ShowNameServiceImpl() {};
		::grpc::Status ShowName(::grpc::ServerContext* context, const ::ShowNameRequest* request, ::Empty* response) override;
};


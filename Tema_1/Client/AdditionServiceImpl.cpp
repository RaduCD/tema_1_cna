#include "AdditionServiceImpl.h"

::grpc::Status AdditionServiceImpl::ShowName(::grpc::ServerContext* context, const::ShowNameRequest* request, ::Empty* response)
{
	std::string clientName = request->clientname();
	std::cout << clientName << "\n";
	return ::grpc::Status::OK;
}
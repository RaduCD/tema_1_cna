#include <iostream>
#include <SumOperation.grpc.pb.h>
#include <ShowNameOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
int main()
{
	grpc_init();
	ClientContext context;
	auto showName_stub = ShowNameOperationService::NewStub(grpc::CreateChannel("localhost:1234",
		grpc::InsecureChannelCredentials()));
	ShowNameRequest nameRequest;
	std::cout << "Please enter your name:";
	std::string name;
	std::cin >> name;
	nameRequest.set_clientname(name);
	Empty response;
	auto status = showName_stub->ShowName(&context, nameRequest, &response);
}